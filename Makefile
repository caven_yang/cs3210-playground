obj-m += procfs1.o
## kbuild command to build a loadable kernel.
## .ko is like .o file, .ko is used to distinguish from .o files.
## 	.ko files carry an additional .modinfo section
##	use >>modinfo *.ko << to show that piece of info.
##	read linux/Documentation/kbuild/makefiles.txt for more about kbuild

##	all modules loaded into the kernel are listed in /proc/modules.
##	>> insmod mod_name.ko << to install the module
##	>> lsmod << to read the module
##	>> rmmod mod_name<< to remove the module
##	read /var/log/messages for >> printk << prints.

##	recompile the kernel to enable >>rmmod -f module << -f means force


all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean


printk:
	tail -n 20 /var/log/messages
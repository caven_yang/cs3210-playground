/*  
 *  hello-2.c - Demonstrating the module_init() and module_exit() macros.
 *  This is preferred over using init_module() and cleanup_module().
 */
#include <linux/module.h>/* Needed by all modules */
#include <linux/kernel.h>/* Needed for KERN_INFO */
#include <linux/init.h>/* Needed for the macros */

static int __init hello_2_init(void)
/** the __init causes the init functions to be discarded and its memory freed
 *  once the init functions finishes for built-in drivers but not loadable
 *  modules.
 *  like __init, __initdata is used to init variables rather than functions.
 */
// in my word, this function is called at >> insmod <<
{
  printk(KERN_INFO "Hello, world 2\n");
  return 0;
}

static void __exit hello_2_exit(void)
/** __exit causes the functions to be omitted when the module is built into
 *  the kernel. It has no effect for loadable modules.
 */
// in my word, this function is called at >> rmmod <<
{
  printk(KERN_INFO "Goodbye, world 2\n");
}

module_init(hello_2_init);
module_exit(hello_2_exit);

/*  
 *  hello-1.c - The simplest kernel module.
 */
#include <linux/module.h>	/* Needed by all modules */
#include <linux/kernel.h>	/* Needed for KERN_INFO */

int init_module(void)
{
	printk(KERN_ALERT "Hello world 1.\n");

	/* 
	 * A non 0 return means init_module failed; module can't be loaded.
	 * if return 1, nothing seems to occur.
	 * by returning -1, insmod fails, saying
	 *     insmod: error inserting 'hello-1.ko': -1 Operation not permitted	 
	 */
	return -1;
}

void cleanup_module(void)
{
	printk(KERN_INFO "Goodbye world 1.\n");
}

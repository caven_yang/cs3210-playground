/**
   a simple example to see what the system calls are invoked by this. Try compile into hello and >>strace ./hello<<
   can write modules to replace the kernel's system calls
 */


#include <stdio.h>

int main(void){
  printf("Hello");
  return 0;
}
